"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def price_kpi(system: LegoAssembly)->float:
    """
    Calculates the total price of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        price_total (float): Sum of prices of all components in the system in Euro

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to price_total()")

    price_total = 0
    for c in system.get_component_list(-1):
        price_total += c.properties["price [Euro]"]
    return price_total

def mass_kpi(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        mass_total (float): Sum of weight of all components in the system in grams

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to mass_kpi()")

    mass_total = 0
    for c in system.get_component_list(-1):
        mass_total += c.properties["mass [g]"]
    return mass_total


def MLT_kpi(system: LegoAssembly)->float:

    MLT = 0
    for c in system.get_component_list(-1):
        delivery_time = c.properties["delivery time [days]"] 
        MLT = max(MLT,delivery_time)
    return MLT


    


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
